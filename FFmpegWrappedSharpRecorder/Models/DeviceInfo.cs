﻿// ReSharper disable once IdentifierTypo
namespace FFmpegWrappedSharpRecorder.Models
{
    internal class DeviceInfo
    {
        public DeviceType DeviceType { get; set; }
        public string DeviceName { get; set; }

        public DeviceInfo()
            : this(DeviceType.Unknown, string.Empty)
        {

        }

        public DeviceInfo(DeviceType deviceType, string deviceName)
        {
            DeviceType = deviceType;
            DeviceName = deviceName;
        }
    }
}
