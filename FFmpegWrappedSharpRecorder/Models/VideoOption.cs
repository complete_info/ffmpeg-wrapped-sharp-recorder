﻿using System;

// ReSharper disable once IdentifierTypo
namespace FFmpegWrappedSharpRecorder.Models
{
    internal class VideoOption
    {
        public string? PixelFormat { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Fps { get; set; }

        public string? Resolution
        {
            get => _resolution;
            set
            {
                //640x480
                _resolution = value;

                if (string.IsNullOrWhiteSpace(value))
                {
                    Width = 0;
                    Height = 0;
                    return;
                }

                try
                {
                    var index = value.IndexOf("x", StringComparison.CurrentCulture);
                    Width = int.Parse(value.Substring(0, index));
                    Height = int.Parse(value.Substring(index + 1));
                }
                catch
                {
                    Width = 0;
                    Height = 0;
                    throw;
                }
            }
        }

        private string? _resolution;
    }
}
