﻿// ReSharper disable once IdentifierTypo
namespace FFmpegWrappedSharpRecorder.Models
{
    internal class EncoderInfo
    {
        public string? EncoderName { get; set; }
        public string? Description { get; set; }

        public string? EncoderString
        {
            get => _encoderString;
            set
            {
                _encoderString = value;
                if (string.IsNullOrWhiteSpace(value))
                {
                    EncoderName = string.Empty;
                    Description = string.Empty;
                    return;
                }

                try
                {
                    //V....D libx264 libx264 H.264 / AVC / MPEG - 4 AVC / MPEG - 4 part 10(codec h264)
                    //V....D libx264rgb libx264 H.264 / AVC / MPEG - 4 AVC / MPEG - 4 part 10 RGB(codec h264)
                    //V....D libopenh264 OpenH264 H.264 / AVC / MPEG - 4 AVC / MPEG - 4 part 10(codec h264)
                    //V....D h264_amf AMD AMF H.264 Encoder(codec h264)
                    //V....D h264_mf H264 via MediaFoundation(codec h264)
                    //V....D h264_nvenc NVIDIA NVENC H.264 encoder(codec h264)
                    //V..... h264_qsv H.264 / AVC / MPEG - 4 AVC / MPEG - 4 part 10(Intel Quick Sync Video acceleration)(codec h264)
                    //V....D libx265 libx265 H.265 / HEVC(codec hevc)
                    //V....D hevc_amf AMD AMF HEVC encoder(codec hevc)
                    //V....D hevc_mf HEVC via MediaFoundation(codec hevc)
                    //V..... hevc_qsv HEVC(Intel Quick Sync Video acceleration) (codec hevc)
                    //V....D libkvazaar libkvazaar H.265 / HEVC(codec hevc)
                    var array = value.Split(' ');
                    if (array.Length < 3)
                    {
                        EncoderName = string.Empty;
                        Description = string.Empty;
                        return;
                    }

                    EncoderName = array[1];
                    Description = string.Join("", array, 2, array.Length - 2);
                }
                catch
                {
                    EncoderName = string.Empty;
                    Description = string.Empty;
                    throw;
                }
            }
        }

        private string? _encoderString;
    }
}
