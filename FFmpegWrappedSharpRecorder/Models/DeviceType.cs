﻿using System.ComponentModel;

// ReSharper disable once IdentifierTypo
namespace FFmpegWrappedSharpRecorder.Models
{
    internal enum DeviceType
    {
        [Description("未知设备")] Unknown = 0,
        [Description("音频设备")] Audio,
        [Description("视频设备")] Video
    }
}
