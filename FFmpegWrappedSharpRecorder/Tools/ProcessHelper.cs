﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

// ReSharper disable once IdentifierTypo
namespace FFmpegWrappedSharpRecorder.Tools
{
    internal class ProcessHelper
    {
        /// <summary>
        /// 执行指定文件
        /// </summary>
        /// <param name="fileName">文件名</param>
        /// <param name="workingDirectory">工作目录</param>
        /// <param name="cmd">命令参数</param>
        /// <param name="encoding">信息编码</param>
        /// <param name="redirectStandardOutput">重定向输出信息</param>
        /// <param name="redirectStandardError">重定向错误信息</param>
        /// <param name="getOutputStream">获取输出流信息</param>
        /// <param name="getErrorStream">获取错误流信息</param>
        /// <param name="createNoWindow">不创建窗口</param>
        /// <returns>执行输出结果</returns>
        public static Tuple<bool, List<string>, string> Execute(
            string fileName,
            string workingDirectory,
            string cmd,
            Encoding encoding,
            bool redirectStandardOutput,
            bool redirectStandardError,
            bool getOutputStream,
            bool getErrorStream,
            bool createNoWindow = true
        )
        {
            var lines = new List<string>();

            try
            {
                //启动主进程
                var process = new Process
                {
                    StartInfo =
                    {
                        UseShellExecute = false, //不使用系统Shell启动
                        FileName = fileName, //文件名
                        WorkingDirectory = workingDirectory, //工作目录
                        Arguments = cmd, //参数
                        CreateNoWindow = createNoWindow //不显示窗口
                    }
                };
                if (redirectStandardOutput)
                {
                    process.StartInfo.RedirectStandardOutput = true; //重定向输出信息
                    process.StartInfo.StandardOutputEncoding = encoding; //输出信息编码
                }

                if (redirectStandardError)
                {
                    process.StartInfo.RedirectStandardError = true; //重定向错误信息
                    process.StartInfo.StandardErrorEncoding = encoding; //错误信息编码
                }

                process.Start();

                //获取cmd输出信息
                if (getOutputStream && redirectStandardOutput)
                {
                    while (!process.StandardOutput.EndOfStream)
                    {
                        var line = process.StandardOutput.ReadLine();
                        if (!string.IsNullOrWhiteSpace(line))
                        {
                            lines.Add(line);
                        }
                    }
                }

                if (getErrorStream && redirectStandardError)
                {
                    while (!process.StandardError.EndOfStream)
                    {
                        var line = process.StandardError.ReadLine();
                        if (!string.IsNullOrWhiteSpace(line))
                        {
                            lines.Add(line);
                        }
                    }
                }

                //等待程序执行完毕退出
                process.WaitForExit();
                process.Close();

                return new Tuple<bool, List<string>, string>(true, lines, string.Empty);
            }
            catch (Exception e)
            {
                return new Tuple<bool, List<string>, string>(false, new List<string>(), e.Message);
            }
        }
    }
}
