﻿using System;
using System.ComponentModel;
using FFmpegWrappedSharpRecorder.ViewModels;

// ReSharper disable once IdentifierTypo
namespace FFmpegWrappedSharpRecorder.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    // ReSharper disable once RedundantExtendsListEntry
    public partial class MainWindow : HandyControl.Controls.Window
    {
        private readonly MainWindowViewModel _viewModel;

        public MainWindow()
        {
            InitializeComponent();

            _viewModel = new MainWindowViewModel();

            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                DataContext = _viewModel;
            }
        }

        private async void MainWindow_OnClosedAsync(object? sender, EventArgs e)
        {
            await _viewModel.DisposeAsync();
        }
    }
}
